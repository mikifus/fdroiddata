Categories:Multimedia
License:GPLv3
Web Site:https://github.com/HoraApps/LeafPic/blob/HEAD/README.md
Source Code:https://github.com/HoraApps/LeafPic
Issue Tracker:https://github.com/HoraApps/LeafPic/issues

Auto Name:LeafPic
Summary:View your images and galleries
Description:
Advanced replacement for the default image gallery.
.

Repo Type:git
Repo:https://github.com/HoraApps/LeafPic/

Build:v0.2.5,1
    commit=d9cc7bccaaad67289cf69d122c698ee056312a64
    subdir=app
    gradle=yes
    forceversion=yes
    prebuild=sed -i -e '/jcenter/amaven { url "https://jitpack.io" }' -e '/dasar/d' -e '/uz.shift/d' -e '/support-v4/acompile "com.github.DASAR:ShiftColorPicker:v0.5@aar"' -e "s/disable 'MissingTranslation'/disable 'MissingTranslation' \ndisable 'ExtraTranslation'\n/g" build.gradle

Build:v0.2.6,2
    commit=200c684915d74ed9277eab78de1a9eef3248a86c
    subdir=app
    gradle=yes
    forceversion=yes
    prebuild=sed -i -e '/jcenter/amaven { url "https://jitpack.io" }' -e '/dasar/d' -e '/uz.shift/d' -e '/support-v4/acompile "com.github.DASAR:ShiftColorPicker:v0.5@aar"' -e "s/disable 'MissingTranslation'/disable 'MissingTranslation' \ndisable 'ExtraTranslation'\n/g" build.gradle

Build:v0.2.7,3
    commit=fc070cc845a32c8ce9632df7c09fcbdd7f3e8220
    subdir=app
    gradle=yes
    forceversion=yes
    prebuild=sed -i -e '/jcenter/amaven { url "https://jitpack.io" }' -e '/dasar/d' -e '/uz.shift/d' -e '/support-v4/acompile "com.github.DASAR:ShiftColorPicker:v0.5@aar"' -e "s/disable 'MissingTranslation'/disable 'MissingTranslation' \ndisable 'ExtraTranslation'\n/g" build.gradle

Build:v0.3,4
    commit=22a3c0d7baa39d43a63e40f81dd9a5af948ad31f
    subdir=app
    gradle=yes
    forceversion=yes
    prebuild=sed -i -e '/jcenter/amaven { url "https://jitpack.io" }' -e '/dasar/d' -e '/uz.shift/d' -e '/support-v4/acompile "com.github.DASAR:ShiftColorPicker:v0.5@aar"' -e "s/disable 'MissingTranslation'/disable 'MissingTranslation' \ndisable 'ExtraTranslation'\n/g" build.gradle

Build:v0.3.1,5
    commit=v0.3.1
    subdir=app
    gradle=yes
    forceversion=yes
    prebuild=sed -i -e '/jcenter/amaven { url "https://jitpack.io" }' -e '/dasar/d' -e '/uz.shift/d' -e '/support-v4/acompile "com.github.DASAR:ShiftColorPicker:v0.5@aar"' -e "s/disable 'MissingTranslation'/disable 'MissingTranslation' \ndisable 'ExtraTranslation'\n/g" build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:v0.3.1
Current Version Code:5
